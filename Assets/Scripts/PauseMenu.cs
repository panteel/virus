﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    GameObject[] pauseObjects;

	GameObject[] loadObjects;
	GameObject[] helpObjects;
	GameObject hide;

	public bool paused;
	scoreText score;


	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
		pauseObjects = GameObject.FindGameObjectsWithTag("Pause");
		loadObjects = GameObject.FindGameObjectsWithTag("Load");
		helpObjects = GameObject.FindGameObjectsWithTag("Help");
		hide = GameObject.FindGameObjectWithTag("Hide");
		score = GameObject.Find ("scoreText").GetComponent<scoreText>();
		hideLoaded();
		hidePaused();
		hideHidden();
		hideHelp();
	}

	// Update is called once per frame
	void Update () {
		
	}


	//Reloads the Level
	public void Reload(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void pauseControl(){
				paused = false;
				Time.timeScale = 1;
				hidePaused();
	}
	public void pauseButton()
	{
		paused = true;
		Time.timeScale = 0;
				showPaused();
	}

	public void showPaused(){
		foreach(GameObject g in pauseObjects){
			g.SetActive(true);
		}
	}

	public void hidePaused(){
		foreach(GameObject g in pauseObjects){
			g.SetActive(false);
		}
	}

	public void showLoaded(){
		foreach(GameObject l in loadObjects){
			l.SetActive(true);
		}
	}
	public void hideLoaded(){
		foreach(GameObject l in loadObjects){
			l.SetActive(false);
		}
	}
	public void showHelp(){
		foreach(GameObject h in helpObjects){
			h.SetActive(true);
		}
	}
	public void hideHelp(){
		foreach(GameObject h in helpObjects){
			h.SetActive(false);
		}
	}
	public void hideHidden()
	{
		if (hide != null)
			hide.SetActive(false);
	}

	public void LoadLevel()
	{
		SceneManager.LoadScene(0);
		Time.timeScale = 1;
	}
	public void Help()
	{
		showHelp();
	}

	public void Quit()
	{
		Application.Quit();
	}
	public void NewGame()
	{
		SceneManager.LoadScene(1);
	}
	public void LoadGame()
	{
		showLoaded();
		
	}

	public void Back()
	{
		hideLoaded();
	}
	public void Back2()
	{
		hideHelp();
	}
	public void Level2()
	{
		if(scoreText.level1Complete)
		{
			SceneManager.LoadScene(2);
		}
	}

	public void Level3()
	{
		if(scoreText.level2Complete)
		{
			SceneManager.LoadScene(3);
		}
	}
}
