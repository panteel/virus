﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beam : MonoBehaviour
{
    Rigidbody2D rb2D;
    AllyChild allyChild;
    Player player;
    GameObject playerCol;
    void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        rb2D.freezeRotation = true;
        allyChild = GameObject.Find ("AllyR").GetComponent<AllyChild>();
        playerCol = GameObject.Find ("PlayerCol");
        player = GameObject.Find ("Player").GetComponent<Player>();
    }
    void Update()
    {
        if(transform.position.magnitude > 1000.0f)
        {
            Destroy(gameObject);
        }
    }

    public void Launch(Vector2 direction, float force)
    {
        rb2D.AddForce(direction * force);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        
        //Player player = GetComponent<Player>();
        //if (playerCol)
        {
            //player.ChangeHealth(-1);
        }
        //Debug.Log("Beam hit " + other.gameObject);
        Destroy(gameObject);
    }
    void OnTriggerEnter2D(Collider2D col){
        if (col.GetComponent<AllyChild>())
        {
            if(allyChild.colliderEnabled)
            {
                //Debug.Log("hit ally");
                Destroy(gameObject);
            }
        }
        if(col.GetComponent<PlayerCol>())
        {
            player.ChangeHealth(-1);
        }        
    }
}
