﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour
{
    public float moveSpeed;
    public bool horizontal;
    public float time = 3.0f;
    public float attackTime = 0.15f;
    public float beamPosition = 0.5f;

    Rigidbody2D rb2D;
    Transform tr;
    float timer;
    float attackTimer;
    int direction = 1;
    private Animator anim;
    private Rigidbody2D target;
    Transform zoom;
    bool isAttacking = false;
    
    public Player player;

    Vector2 lookDir = new Vector2(1,0);
    
    public GameObject beamPrefab;

    public bool beamLeft;
    public bool beamRight;
    public int maxHealth = 3;
    int currentHealth;
    public static float distance;
    

    void Start()
    {
        target = GameObject.FindGameObjectWithTag ("Player").GetComponent<Rigidbody2D>();
        zoom = GameObject.FindGameObjectWithTag ("Player").GetComponent<Transform>();
        player = GameObject.Find ("Player").GetComponent<Player>();
        rb2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        tr = GetComponent<Transform>();
        timer = time;
        attackTimer = attackTime;
        currentHealth = maxHealth;
    }

    void Update()
    {   
        distance = Vector2.Distance(tr.position,zoom.position);
        if(currentHealth != 0)
        {
            if (!isAttacking && distance > 3)
            {
                //code below is used when robot is not near player
                timer -= Time.deltaTime;

                if (timer<0)
                {
                    direction = -direction;
                    timer = time;
                }
                Vector2 position = rb2D.position;

                if (horizontal)
                {
                    position.x = position.x + Time.deltaTime * moveSpeed*direction;
                    anim.SetFloat("Direction", direction);
                    //if collision with non-player, turn direction
                }
                else
                {
                    position.y = position.y + Time.deltaTime * moveSpeed*direction;
                    anim.SetFloat("Direction", direction);
                    //same as above
                }

                rb2D.MovePosition(position);
            }

            if (distance < 3)
            {
                Vector2 position = rb2D.position;

                if ( target.position.x < position.x)
                {
                    anim.SetFloat("Direction", -1);
                }
                else
                {
                    anim.SetFloat("Direction", 1);
                }
                
                position = Vector2.MoveTowards(position, target.position, moveSpeed*Time.deltaTime);
                
                rb2D.MovePosition(position);
                
                    attackTimer -= Time.deltaTime;

                    if (attackTimer<0)
                    {
                        Launch();
                        attackTimer = attackTime;
                    }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "AllyBeam" || other.gameObject.tag == "Beam")
        {
            rb2D.velocity = Vector2.zero;
        }
    }
    public void ChangeHealth(int amount)
    {
        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);
        Debug.Log(currentHealth + "/" + maxHealth);
        if(currentHealth == 0)
        {
            anim.SetTrigger("Dead");
            scoreText.enemies -= 1;
            rb2D.simulated = false;
        }
    }

    public void Launch()
    {
        GameObject beamObject = Instantiate(beamPrefab, rb2D.position + Vector2.up *beamPosition, Quaternion.identity);

        Beam beam = beamObject.GetComponent<Beam>();
        Vector2 position = rb2D.position;
        if ( target.position.x < position.x)
            {
                lookDir = new Vector2(-1,0);
                beam.Launch(lookDir, 200);
                beamLeft = true;
                
            }
            else
            {
                lookDir = new Vector2(1,0);
                beam.Launch(lookDir, 200);
                beamRight = true;
            }
    }
}
