﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public Player player;
    public float restartTime = 3f;
    Animator anim;
    float restartTimer;
    //public scoreText scoreText4444;
    public Level1Text level1;
    
    void Awake ()
    {
        anim = GetComponent<Animator>();
        level1 = GameObject.Find("Level1CompleteText").GetComponent<Level1Text>();
    }


    void Update ()
    {
        if(player.currentHealth <= 0)
        {
            anim.SetTrigger ("GameOver");
            //Debug.Log("player dead");

            restartTimer += Time.deltaTime;

            if(restartTimer >= restartTime)
            {
                //Debug.Log("Restart");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }

        if(scoreText.level1Complete  && scoreText.enemies == 0)
		{
            level1.sceneID = 1;
			anim.SetTrigger("LevelComplete");
            //anim.ResetTrigger("LevelComplete");
		}

        if(scoreText.level2Complete  && scoreText.enemies == 0)
		{   
            level1.sceneID = 2;
			anim.SetTrigger("LevelComplete");
		}
        if(scoreText.level3Complete  && scoreText.enemies == 0)
		{   
            level1.sceneID = 3;
			anim.SetTrigger("LevelComplete");
		}
    }
}
