﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class scoreText : MonoBehaviour
{
    public static int enemies;
    Text text = null;
    public float waitTime = 2.0f;
    float waitTimer;
    public static bool level1Complete;
    public static bool level2Complete;
    public static bool level3Complete;
    public int sceneID;
    // Start is called before the first frame update

    void Awake()
    {
        sceneID = SceneManager.GetActiveScene().buildIndex;
        if (sceneID == 1)
        {
           enemies = 5; 
        }
        if (sceneID == 2)
        {
           enemies = 10; 
        }
        if (sceneID == 3)
        {
           enemies = 15; 
        }
    }
    void Start()
    {
        text = GetComponent<Text>();
        waitTimer = waitTime;  
    }

    // Update is called once per frame
    void Update()
    {
        sceneID = SceneManager.GetActiveScene().buildIndex;
        //Debug.Log(sceneID);
        if (text != null)
        {
            text.text = "Enemies Left: " + enemies;
        }
        if (sceneID == 1 && enemies == 0)
        {
                waitTimer -= Time.deltaTime;
                level1Complete = true;
            

                if (waitTimer<0)
                {
                
                SceneManager.LoadScene(2);
                enemies = -1;
                waitTimer = waitTime;
                }
                
            
        }
        else if (sceneID == 2 && enemies == 0 && level1Complete)
            {
                waitTimer -= Time.deltaTime;
                level2Complete = true;

                if (waitTimer<0)
                {
                SceneManager.LoadScene(3);
                enemies = -1;
                waitTimer = waitTime;
                }
            }

        else if (sceneID == 3 && enemies == 0 && level2Complete)
            {
                waitTimer -= Time.deltaTime;
                level3Complete = true;

                if (waitTimer<0)
                {
                SceneManager.LoadScene(0);
                enemies = -1;
                waitTimer = waitTime;
                }
            }
    }
}
