﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ally : MonoBehaviour
{
    public GameObject allyBeamPrefab;
    private Rigidbody2D target; 
    private Animator anim;
    Rigidbody2D rb2D;
    public float moveSpeed;
    public bool istouching = false;
    public bool isAwake = false;
    float timer;
    public float time;
    public float axisX;
    public float axisY;
    public float attackTime = 0.0f;
    public float beamPosition = 0.5f;
    float attackTimer;
    


    Vector2 displacementPosition;
    
    void Start()
    {
        target = GameObject.FindGameObjectWithTag ("Player").GetComponent<Rigidbody2D>(); 
        anim = GetComponentInChildren<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
        timer = time;
        attackTimer = attackTime;

    }

    void Update()
    {
        displacementPosition = new Vector2(target.position.x +axisX, target.position.y +axisY);
        if (Input.GetKeyDown(KeyCode.Space) && istouching)
            {
                anim.SetTrigger("Wake");
                isAwake = true;
            }

        if (isAwake)
        {
            timer -= Time.deltaTime;

            if (timer < 0)
            {
                Vector2 position = rb2D.position;
                position = Vector2.MoveTowards(position, displacementPosition, moveSpeed*Time.deltaTime);
                rb2D.MovePosition(position);
            }

            if (Input.GetKeyDown(KeyCode.M))
            {
                //anim.SetTrigger("Attack");
                attackTimer -= Time.deltaTime;

                    if (attackTimer<0)
                    {
                        Launch();
                        attackTimer = attackTime;
                        anim.SetTrigger("Attack");
                    }
            }
        }

            
    }

    public void Launch()
    {
        GameObject allyBeamObject = Instantiate(allyBeamPrefab, rb2D.position + Vector2.up *beamPosition, Quaternion.identity);

        AllyBeam allyBeam = allyBeamObject.GetComponent<AllyBeam>();
        Vector2 position = rb2D.position;
                //lookDir = new Vector2(-1,0);
                allyBeam.Launch(new Vector2(1,0), 200);
    }


    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Player>())
        {
            istouching = true;
        }
    }   

    public void OnTriggerExit2D(Collider2D col)
    {
        if (col.GetComponent<Player>())
        {
            istouching = false;
        }
    }
}
