﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BGM : MonoBehaviour
{   int sceneID;
    AudioSource audios;
    private static BGM instance = null;
    public static BGM Instance
    {
        get { return instance; }
    }

     void Awake() {
        audios = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        sceneID = SceneManager.GetActiveScene().buildIndex;
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        if (sceneID == 0)
        {
            //audios.mute = true;
            audios.Stop();
        }
        else
        {
            //audios.mute = false;
            if (!audios.isPlaying)
            {
                audios.Play();
            }
        }
    }
}
