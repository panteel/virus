﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyBeam : MonoBehaviour
{
    Rigidbody2D rb2D;
    Robot robot;
    
    void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        rb2D.freezeRotation = true;
    }
    void Update()
    {
        if(transform.position.magnitude > 1000.0f)
        {
            Destroy(gameObject);
        }
    }

    public void Launch(Vector2 direction, float force)
    {
        rb2D.AddForce(direction * force);
    }

    void OnCollisionEnter2D(Collision2D other)
    {   
        //Robot robot = other.collider.GetComponent<Robot>();
        //if (robot)
        {
            //robot.ChangeHealth(-1);
            //Debug.Log("robot hit");
        }
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D col){
        RobotCol robotCol = col.GetComponent<RobotCol>();
        if (robotCol)
        {
            robot = robotCol.GetComponentInParent<Robot>();
            if (robot)
            {
                robot.ChangeHealth(-1);
                Destroy(gameObject);
            }            
        }                
    }
}
