﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyChild : MonoBehaviour
{
    Collider2D col;
    Ally ally;
    public bool colliderEnabled;
    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<Collider2D>();
        ally = GameObject.Find ("AllyR_Root").GetComponent<Ally>();
        col.enabled = false;

        if(col.enabled == false)
        {
            Debug.Log("col for allyChild is disabled");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (ally.isAwake)
        {
            col.enabled=true;
            Debug.Log("col for allyChild is enabled");
            colliderEnabled = true;

        }
    }
}
