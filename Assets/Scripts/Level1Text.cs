﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Level1Text : MonoBehaviour
{
    Text text = null;
    public int sceneID;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        //sceneID = SceneManager.GetActiveScene().buildIndex;

        if (text != null)
        {
            text.text = "Level " + sceneID + " Complete";
        }
    }
}
