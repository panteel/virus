﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int maxHealth = 3;
    public int currentHealth;
    Rigidbody2D rb2D;
    public float moveSpeed = 1.0f;
    Animator animator;
    Vector2 lookDirection = new Vector2(1,0);
	PauseMenu pause;

    //Transform tr;
    
    void Start()
    {
        animator = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
        currentHealth = maxHealth;
        pause = GameObject.Find ("UI Manager").GetComponent<PauseMenu>();
    }

    void Update()
    {   
        if (pause.paused == false)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            Vector2 move = new Vector2(horizontal,vertical);

            if(!Mathf.Approximately(move.x, 0.0f))
            {
                lookDirection.Set(move.x, 0.0f);
                lookDirection.Normalize();
            }
            
            Vector2 position = rb2D.position;
            position = position + move* moveSpeed * Time.deltaTime;
            rb2D.MovePosition(position);

            animator.SetFloat("Direction", lookDirection.x);
            animator.SetFloat("Speed", move.magnitude);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                animator.SetTrigger("Kick");
            }
        }
    }

    public void ChangeHealth(int amount)
    {
        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);
        HealthBar.instance.SetValue(currentHealth / (float)maxHealth);
    }
}
