﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassHasItem : MonoBehaviour
{
    Animator anim;
    Player player;
    bool istouching;
    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
        player = GameObject.Find ("Player").GetComponent<Player>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && istouching)
        {
            player.ChangeHealth(1);
            anim.SetBool("Collected", true);
        }
    }
   private void OnTriggerEnter2D(Collider2D other)
   {
       Player player = other.GetComponent<Player>();

       if (player != null)
        {
            if(player.currentHealth  < player.maxHealth)
            {
                istouching = true;
            }
                
        }
   }
}
